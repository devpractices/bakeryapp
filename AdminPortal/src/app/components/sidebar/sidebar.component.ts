import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
     //{ path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
   // { path: '/table-list', title: 'Table List',  icon:'content_paste', class: '' },
    { path: '/category-list', title: 'Categories',  icon:'category', class: '' },
    { path: '/product-list', title: 'Products',  icon:'grid_on', class: '' },
    { path: '/offers-list', title: 'Offers',  icon:'local_offer', class: '' },
    { path: '/orders-list', title: 'Orders',  icon:'local_grocery_store', class: '' },
    { path: '/user-list', title: 'Users',  icon:'group', class: '' },
    { path: '/messages-list', title: 'Messages',  icon:'email', class: '' },
    { path: '/banner-list', title: 'Banners',  icon:'images', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
