import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { DataService} from '../services/data.service'
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
declare var $: any;
const swal = require('sweetalert');
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../services/user-service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [DataService, UserService]
})
export class UserListComponent implements OnInit {
  @ViewChild(ModalDirective, {read: true, static: false }) addUserModal:ModalDirective;
  loading = false;
  users = [];
  userForm: FormGroup;
  totalRec : number;
  page: number = 1;
  baseUrl=null;
  nameControl:FormControl;
  emailControl:FormControl;
  dobControl:FormControl;
  genderControl:FormControl;
  userNameControl:FormControl;
  mobileControl:FormControl;
  passwordControl:FormControl;
  hidePassword: any = false;
  mode = "new";
  public loggedInUser = JSON.parse(sessionStorage.getItem('loggedInUser'));
  selectedUser = null

  public popoverTitle:string='Confirmation';
  public popoverMessage:string='Are you sure you want to delete User?';
 public confirmClicked:boolean=false;
 public cancleClicked:boolean=false;

  constructor(private userservice:UserService, private formBuilder:FormBuilder, private dataService:DataService,private spinnerService: Ng4LoadingSpinnerService) { 
    this.baseUrl = this.dataService.getBaseUrl();
    this.nameControl = this.formBuilder.control('', Validators.required);
    this.emailControl = this.formBuilder.control('', Validators.required);
    // this.dobControl = this.formBuilder.control('', Validators.required);
    // this.genderControl = this.formBuilder.control('', Validators.required);
    this.userNameControl = this.formBuilder.control('',Validators.required);
    if(this.mode !== 'new'){
      this.passwordControl = this.formBuilder.control('');
    } else {
      this.passwordControl = this.formBuilder.control('',Validators.required);
    }
    
    this.mobileControl = this.formBuilder.control('');
    

    this.userForm = this.formBuilder.group({
      'name': this.nameControl,
      'userName': this.userNameControl,
      'email': this.emailControl,
      // 'dob': this.dobControl,
      // 'gender' : this.genderControl,
      'mobile' : this.mobileControl,
      'password' : this.passwordControl
    });
  }
  
  ngOnInit() {
    this.getUsers();
    this.userForm.reset(); 
  }


  pageChange(pageNo) {
    this.page = pageNo  
    this.getUsers();  
  }



  getUsers() {
    this.spinnerService.show();
    
    this.userservice.getUsers({}).subscribe(         
      (data: any) => {
       
        this.users = data.users;
       
        this.spinnerService.hide();
        this.totalRec = data.total;    
      },
      error => {
        this.spinnerService.hide();
        
      }
    );
  }

  submitForm($ev) {
    $ev.preventDefault();
    for (let c in this.userForm.controls) {
      this.userForm.controls[c].markAsTouched();
    }

    if (this.userForm.valid) {
      this.saveUser();
    }
  }

  saveUser() {
    var userDto = {
      "name": this.userForm.get('name').value,
      "email": this.userForm.get('email').value,
      "mobile": this.userForm.get('mobile').value,
      "roleId": 2,
      "user_id": this.userForm.get('userName').value,
      "password": this.userForm.get('password').value,
    };

      // // "dob": this.userForm.get('dob').value,
      // "gender": this.userForm.get('gender').value

    this.userservice.saveUser(userDto).subscribe(
      (data: any) => {
        this.loading = false; 
        this.dataService.showNotification("success", "User created successfully...")
        this.hideModal();
        this.page = 1;
        this.getUsers();
      },
      error => {
        this.loading = false;
      }
    );
    
  }

  showModal(mode, item){
    $("#addUserModal").modal({backdrop: 'static', keyboard: false, show: true});  
    if(mode === 'new'){
      this.mode = "new"
      this.hidePassword = false;
    } else {
      this.mode = "edit"
      this.hidePassword = true;
      this.selectedUser = item;
      if(item){
        this.userForm.patchValue({
          'name': item.name ,
          'user_id':item.userName,
          'email':item.email,
          'mobile': item.mobile
          // 'dob':item.date_of_birth,
          // 'gender' : item.gender,
        });
      }
    }
  }

  hideModal(){
    this.userForm.reset();
    $("#addUserModal").modal("hide");   
  }


  deleteUser(id) {
    this.userservice.deleteUser(id).subscribe(
      (data: any) => {
        this.loading = false; 
        this.dataService.showNotification("success", "User deleted successfully...");
        this.page = 1;
        this.getUsers();       
      },
      error => {
        this.loading = false;
      }
    );
  }

  updateUser() {
    var userDto = {
      "id": this.selectedUser.id,
      "name": this.userForm.get('name').value,
    //   "UserName": this.userForm.get('userName').value,
      "email": this.userForm.get('email').value ,
      "dob": this.userForm.get('dob').value,
      "gender": this.userForm.get('gender').value    
    };
    
    this.userservice.updateUser(userDto).subscribe(
      (data: any) => {
        this.loading = false; 
        this.dataService.showNotification("success", "User details has been updated successfully....")
        this.hideModal();
        this.selectedUser = null;
        this.page = 1;
        this.getUsers();
      },
      error => {
        this.loading = false;
      }
    );
    
  }

  imageUrlFormater(imagePath){
    var url = this.baseUrl;
    if(imagePath) { 
      return url.substr(0,url.lastIndexOf('/')) +imagePath;    
    } else {
      return '/assets/img/user_na.png'//url.substr(0,url.lastIndexOf('/')) +imagePath;    
    }
    
  }

}
