var pool = require('../../config');
var _ = require('underscore');

const createCategory = (request, response) => {
    var name = JSON.parse(request.body.name);
    
    pool.query('INSERT INTO category (name, created_on) VALUES ($1,now()) RETURNING id', [name,], (error, result) => {
        if (error) {
          response.status(500).json({"status": "Fail", "message":"Error occured while creating category.Please try again."});
        }
        
        categoryId = result.rows[0].id;
        if(request.file){
          uploadImage(categoryId, request.file)
        }
        response.status(200).json({"status": "Success", "message":"Category created successfully."});
    })
}

function uploadImage(categoryId, file){
  var entityType = 'category',
      entityId = categoryId,
      imgName = file.filename,
      imgPath = '/categoryImg/' + file.filename;

  pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType,entityId, imgName, imgPath], (error, result) => {
      if (error) {
          throw error
      }
      return true;
  })
}

const getCategories = (request, response) => {
    var entityType = 'category';
    var query = `SELECT c.id, c.name, i.img_name,i.img_path  FROM category c
                left join images i on i.entity_id = c.id and i.entity_type = '${entityType}'
                ORDER BY id ASC`

    pool.query(query, (error, results) => {
        if (error) {
          //throw error
          response.status(500).json({"status": "Fail", "message":"Error occured while getting category.Please try again."});
        } else {
            var categories = [];
            _.each(results.rows, function(category) {              
              var categoryDetails = { 
                  id: category.id,
                  name: category.name, 
                  imageName: category.img_name,
                  imagePath: category.img_path
              }
              categories.push(categoryDetails);
            });
            response.status(200).json(categories);
        }
    })
}

const createSubCategory = (request, response) => {
    const {name, categoryId} = request.body    
    pool.query('INSERT INTO sub_category (name, parent, created_on) VALUES ($1,$2,now())', [name,categoryId], (error, result) => {
      if (error) {
        throw error
      }
      console.log("res : ",  result);
      response.status(201).send(`Sub Category added with ID: ${result.insertId}`)
    })
}


const getSubCategories = (request, response) => {
    pool.query('SELECT * FROM sub_category ORDER BY id ASC', (error, results) => {
        if (error) {
        throw error
        }
        response.status(200).json(results.rows)
    })
}

const getSubCategoriesById = (request, response) => {
    const parent = parseInt(request.params.parent)

    pool.query('SELECT * FROM sub_category WHERE parent = $1', [parent], (error, results) => {
        if (error) {
        throw error
        }
        response.status(200).json(results.rows)
    })
}

const createFlavour = (request, response) => {
    const {name} = request.body    
    pool.query('INSERT INTO flavour (name, created_on) VALUES ($1,now())', [name], (error, result) => {
      if (error) {
        throw error
      }
      
      response.status(201).send(`Flavour added with ID: ${result.insertId}`)
    })
} 

const getFlavours = (request, response) => {
    pool.query('SELECT * FROM flavour ORDER BY id ASC', (error, results) => {
        if (error) {
        throw error
        }
        response.status(200).json(results.rows)
    })
}

const referPointTypeCreate = (request, response) => {
  const {type, points} = request.body    
  pool.query('INSERT INTO refer_type (type, points, created_on) VALUES ($1, $2,now())', [type, points], (error, result) => {
    if (error) {
      response.status(500).json({"status": "Fail", "message":"Error occured while creating refral type.Please try again."});
    }

    response.status(200).json({"status": "Success", "message":"Referal type created successfully."});
  })
}


const getCounts = (request, response) => {
  var counts = [];

  pool.query(` select count(*) product from product where is_active=true;select count(*) category from category;select count(*) offer from offer where is_active=true; select count(*) orders from orders where status not in ('DELIVERED', 'CANCELED', 'INCOMPLETE')`, function(err, res) {
    _.each(res, function(result, index) {
        if(!_.isEmpty(result.rows)){
          counts.push(result.rows[0]);          
        }
    });
    response.status(200).json(counts);
  });  
}

module.exports = {
    createCategory,
    getCategories,
    createSubCategory,
    getSubCategories,
    getSubCategoriesById,
    createFlavour,
    getFlavours,
    referPointTypeCreate,
    getCounts
}