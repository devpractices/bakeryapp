var pool = require('../../config');
var _ = require('underscore');
var moment = require('moment');

const createOffer = (request, response) => {
    const {offerValue, offerType, offerDesc, offerCode, validFrom, validTo, point} = JSON.parse(request.body.offerdetails);
    var offerId = null;


    pool.query(`select * from offer where offer_code='${offerCode}'` , (offerExistError, offerExistResult) => {
        if(offerExistError){; 
            console.log(" offerExistError: ", offerExistError);
            response.status(500).json({"status": "Fail", "message":"Error occured while checking existing offer.Please try again."});
        } else{
            if(offerExistResult && offerExistResult.rows.length>0){
                response.status(500).json({"status": "Success", "message":"Offer code is already registerd in system.Please try again with new code."});
            } else {
                pool.query('INSERT INTO offer (offer_value,offer_type,offer_desc,offer_code, from_date,to_date, point,created_on) VALUES ($1, $2, $3, $4, $5,$6,$7 ,now()) RETURNING id', [offerValue, offerType, offerDesc,offerCode, validFrom, validTo, point], (error, result) => {
                    if (error) {
                      response.status(500).json({"status": "Fail", "message":"Error occured while creating new offer.Please try again."});
                    }
              
                    offerId = result.rows[0].id;
                    if(request.file){
                      uploadImage(offerId, request.file, response)
                    }
              
                    response.status(200).json({"status": "Success", "message":"Offer created successfully."});
                })
            }
        }
    });

    
}


function uploadImage(offerId, file, res){
    var entityType = 'offer',
        entityId = offerId,
        imgName = file.filename,
        imgPath = '/offerImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType,entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}

const updateOffer = (request, response) => {
    const {offerValue, offerType, offerDesc, id, offerCode, validFrom, validTo, point} = request.body;
    
    pool.query("UPDATE offer SET offer_value = $1,offer_type =$2 ,offer_desc = $3, offer_code=$5, from_date=$6, to_date=$7, point=$8  WHERE id = $4",
         [offerValue, offerType, offerDesc, id,offerCode, validFrom, validTo, point], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while updating offer details.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"Offer details has been updated successfully."});
    });
}

const updateOfferImage = (request, response) => {
    const {id} = JSON.parse(request.body.offerdetails);
    var file = request.file;

    var entityType = 'offer',
        entityId = id,
        imgName = file.filename,
        imgPath = '/offerImg/' + file.filename;

    pool.query("UPDATE images SET img_name = $1, img_path = $2 WHERE entity_id = $3 and entity_type = $4",
         [imgName, imgPath, entityId, entityType], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while updating offer image.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"Offer image updated successfully."});
    });
}

const mapOfferProduct = (request, response) => {
    const {productId, offerId, from, to} = request.body
    pool.query('INSERT INTO product_offer (product_id,offer_id,from_date, to_date, created_on) VALUES ($1, $2, $3, $4,now())', [productId, offerId, from, to], (error, result) => {
      if (error) {
        throw error
      }
      response.status(201).send(`Maped Offer and product successfully: ${result.insertId}`)
    })
}

const getOffers = (request, response) => {
    const { start, limit} = request.body
    var offers = [],
    entityType = 'offer';

    var query = `SELECT o.id offer_id, o.offer_value, o.offer_type,o.offer_desc, o.offer_code, o.from_date, o.to_date, o.point, i.img_name, i.img_path, count(o.*) OVER() AS full_count  FROM offer o
                 left join images i on i.entity_id = o.id and i.entity_type = '${entityType}' where o.is_active = true
                 ORDER BY o.id ASC`;

    console.log(" query : ", query);

    if(_.has(request.body, "start") && _.has(request.body, "limit")){
        query = `SELECT o.id offer_id, o.offer_value, o.offer_type,o.offer_desc, o.offer_code, o.from_date, o.to_date, o.point, i.img_name, i.img_path, count(o.*) OVER() AS full_count  FROM offer o
            left join images i on i.entity_id = o.id and i.entity_type = '${entityType}'
            where o.is_active = true
            ORDER BY o.id ASC
            OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`;
    }
    
    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var totalCount = 0;
        _.each(results.rows, function(offer) {
            var offerDetails = { 
                o_id: offer.offer_id,
                offerValue: offer.offer_value,
                offerType: offer.offer_type,
                offerDesc: offer.offer_desc,
                imageName: offer.img_name,
                imagePath: offer.img_path,
                offerCode: offer.offer_code,
                point: offer.point,
                offerValidFrom : moment(offer.from_date).format('YYYY-MM-DD'), 
                offerValidTo : moment(offer.to_date).format('YYYY-MM-DD')
            }
            totalCount = offer.full_count;
            offers.push(offerDetails);            
        });        
        response.status(200).json({totalCount: totalCount ,offers: offers});
    })
    
}

const getOfferById = (request, response) => {
    const id = parseInt(request.params.id)
    var entityType = 'offer';
    var query = `SELECT o.id offer_id, o.offer_value, o.offer_type,o.offer_desc,o.offer_code, o.from_date, o.to_date, o.point, i.img_name, i.img_path  FROM offer o
                left join images i on i.entity_id = o.id and i.entity_type = '${entityType}'
                WHERE id = $1
                ORDER BY id ASC`

    pool.query(query, [id], (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var offerDetails = {};
        _.each(results.rows, function(offer) {
            offerDetails = { 
                o_id: offer.id,
                offerValue: offer.offer_value,
                offerType: offer.offer_type,
                offerDesc: offer.offer_desc,
                imageName: offer.img_name,
                imagePath: offer.img_path,
                offerCode: offer.offer_code,
                point: offer.point,
                offerValidFrom : moment(offer.from_date).format('YYYY-MM-DD'), 
                offerValidTo : moment(offer.to_date).format('YYYY-MM-DD')
            }
        });

        response.status(200).json(offerDetails)
    })
}


const getOffersByProduct = (request, response) => {
    var productId = parseInt(request.params.productId);
    const { start, limit} = request.body
    var entityType = 'offer';
    var offers= []
    var query = `SELECT o.id, o.offer_value, o.offer_type, o.offer_desc, po.product_id, po.from_date, po.to_date, i.img_name, i.img_path, count(o.*) OVER() AS full_count FROM offer o
                inner join product_offer po on po.offer_id = o.id
                left join images i on i.entity_id = o.id and i.entity_type = '${entityType}'
                where po.product_id = ${productId}
                ORDER BY o.id ASC` 

    if(_.has(request.body, "start") && _.has(request.body, "limit")){
        query = `SELECT o.id, o.offer_value, o.offer_type, o.offer_desc, po.product_id, po.from_date, po.to_date, i.img_name, i.img_path, count(o.*) OVER() AS full_count FROM offer o
                inner join product_offer po on po.offer_id = o.id
                left join images i on i.entity_id = o.id and i.entity_type = '${entityType}'
                where po.product_id = ${productId}
                ORDER BY o.id ASC
                OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }
    
        var totalCount = 0;
        _.each(results.rows, function(offer) {
            var offerDetails = { 
                o_id: offer.id,
                offerValue: offer.offer_value,
                offerType: offer.offer_type,
                offerDesc: offer.offer_desc,
                productId: offer.product_id,
                imageName: offer.img_name,
                imagePath: offer.img_path,
                offerValidFrom : moment(offer.from_date).format('YYYY-MM-DD'), 
                offerValidTo : moment(offer.to_date).format('YYYY-MM-DD')
            }
            totalCount = offer.full_count;
            offers.push(offerDetails);            
        });

        response.status(200).json({totalCount: totalCount ,offers: offers});
    })
    
}


const validateOfferCode = (request, response) => {
    const {offerCode} = request.body

    var query = `select * from offer where offer_code = '${offerCode}'`
    pool.query(query, (error, results) => {
        if(error){
            console.log(" error code ", error);
        }

        if(_.isEmpty(results.rows)){
            response.status(200).json({"status": "Success", "message":"Invalid offer code"});
        } else {    
            var offerObj = results.rows[0];
            var offerDetails = { 
                o_id: offerObj.offer_id,
                offerValue: offerObj.offer_value,
                offerType: offerObj.offer_type,
                offerDesc: offerObj.offer_desc,
                offerCode: offerObj.offer_code,
                point: offerObj.point,
                offerValidFrom : moment(offerObj.from_date).format('YYYY-MM-DD'), 
                offerValidTo : moment(offerObj.to_date).format('YYYY-MM-DD')
            }

            if(offerObj.is_active){
                response.status(200).json(offerDetails);
            } else {
                response.status(200).json({"status": "Success", "message":"Offer expired."});
            }
            
        }

    
    });

}

const deleteOffer = (request, response) => {
    var offerId =  parseInt(request.params.id);
    var checkOfferInOrder = `select count(o.*) orderactivecount from orders o
                               where o.offer_id = ${offerId}`
    var today = moment(new Date()).format('YYYY-MM-DD');
    
    pool.query(checkOfferInOrder,(checkError, checkResult) => {
        if (checkError) {
            response.status(500).json({"status": "Fail", "message":"Error occured while checking offer used is in active order.Please try again."});
        }
        if(!_.isEmpty(checkResult.rows)){
            if(parseInt(checkResult.rows[0].orderactivecount) > 0){
                response.status(500).json({"status": "Fail", "message":"This offer is applied on some orders. Please change the status of order and try again"});
            } else {
                pool.query("UPDATE offer SET is_active = false , to_date = $2, updated_on = 'now()' WHERE id = $1",
                    [offerId, today], (error, result) => {
                    if (error) {
                        response.status(500).json({"status": "Fail", "message":"Error occured while deleting offer.Please try again."});
                    }      
                    response.status(200).json({"status": "Success", "message":"Offer deleted successfully."});
                });
            }
        } else {
            console.log(" In order delete else ");
            response.status(500).json({"status": "Fail", "message":"Unexpedted erorr"});
        }
    });    
}

module.exports = {
    createOffer,
    updateOffer,
    updateOfferImage,
    getOffers,
    getOfferById,
    mapOfferProduct,
    validateOfferCode,
    getOffersByProduct,
    deleteOffer 
}