var pool = require('../../config');
var _ = require('underscore');
var moment = require('moment');

const createTemplateMessage = async (request, response) => {
    const { msg, msg_type } = request.body;
    const query = {
        text: 'INSERT INTO template_message (msg,msg_type,created_on) VALUES ($1, $2, now())',
        values: [msg, msg_type]
    }

    await pool.query(query)
        .then(result => {
            console.log(result.rowCount);
            response.status(200).json({ "status": "Success", "message": "Template message added successfully." })
        })    
        .catch(error => {
            console.log("error : ", error);
            response.status(500).json({ "status": "Fail", "message": "Error occured while adding new template.Please try again." });
        });
}

const updateTemplateMessage = (request, response) => {
    const { msg, msg_type, id } = request.body;

    pool.query("UPDATE template_message SET msg = $1, msg_type = $2, updated_on='now()' WHERE id = $3",
        [msg, msg_type, id], (error, result) => {
            if (error) {
                response.status(500).json({ "status": "Fail", "message": "Error occured while updating template details.Please try again." });
            }
            response.status(200).json({ "status": "Success", "message": "Template message details has been updated successfully." });
        });
}

const getTemplateMessages = async (request, response) => {
    const query = `SELECT * FROM template_message`;
    console.log("query : ", query);
    await pool.query(query).then(results => {
        // console.log(results.rows);
        response.status(200).json(results.rows);
    }).catch(error => {
        response.status(500).json({ "status": "Fail", "message": "Something went to wrong.Please contact to administrator." });
    });
}


const getTemplateMessageById = async (request, response) => {
    const id = parseInt(request.params.id)

    const query = `SELECT * FROM template_message WHERE id=$1`;
    console.log("query : ", query);
    await pool.query(query, [id]).then(results => {
        // console.log(results.rows);
        response.status(200).json(results.rows);
    }).catch(error => {
        response.status(500).json({ "status": "Fail", "message": "Something went to wrong.Please contact to administrator." });
    });
}

module.exports = {
    createTemplateMessage,
    updateTemplateMessage,
    getTemplateMessages,
    getTemplateMessageById
}