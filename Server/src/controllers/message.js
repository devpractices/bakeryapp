const pool = require('../../config');
const rp = require('request-promise-native');
const { formRequest, insertOTP, getOTP } = require('../utils/commonutil');
var _ = require('underscore');

const sendMessage = async (request, response) => {
  
  const { msg_type, to, from } = request.body;
  console.log(msg_type, to, from);
  
  try {
    const query = { text: 'SELECT * FROM template_message WHERE msg_type=$1', values: [msg_type] };
    console.log(query);
    
    const result  = await pool.query(query);
    console.log("number or rows ", result.rowCount);
    
    if (!(msg_type || from || to ) || result.rowCount === 0) {
     return response.status(400).json({
        "type": "error",
        "message": "Invalid message type or invalid mobile number."
      });
    } 

    // form a request for send message to msg91
    const req = formRequest(result.rows[0].msg, to);
    rp(req).then(res => {
      console.log("res", res);
      
      pool.query('INSERT INTO sms_details (from_no, sms_type, to_no, sent_time) VALUES ($1,$2,$3,now())', [from, msgno, to.join(',')], (error, result) => {
        if (error) {
          throw error
        }
        console.log("res : ", JSON.stringify(result));
      });
      console.log("after insert statment");
      response.status(200).json({ status: "Success", message: "Message sent successfully" });
      console.log("After send statement");
    }).catch(err => {
      console.log("Error ", err.message);
      response.status(err.statusCode).json(err.error);
    });
  } catch(err) {
      response.status(400).json({
        "type": "error",
        "message": err
      });
  }
}

const sendOTP = async (request, response) => {
  console.log(request.body);
  const otp = Math.floor(1000 + Math.random() * 9111);
  const { mobile_no } = request.body;
  try {
    
    pool.query('SELECT * FROM account WHERE user_id = $1', [mobile_no], (userExistError, userExistResults) => {
      if(userExistError){
          console.log(" errors : ", userExistError);
          response.status(500).json({"status": "Fail", "message":"Error occured while checking existing user.Please try again."});
      }
      
      if(_.isEmpty(userExistResults.rows)){
          response.status(500).json({"status": "Fail", "message":"Mobile number not found in system.Please sign up."});
      } else {
        var userObj = userExistResults.rows[0];
        if(userObj.is_active === false && userObj.is_deleted === true){
          response.status(500).json({"status": "Fail", "message":"Your account is deactivated. Please contact to administrator for activate your account."});
        } else {
          pool.query('INSERT INTO otp_verification (mobile_no, otp, otp_timestamp) VALUES ($1,$2,now())', [mobile_no, otp], (error, result) => {
            if (error) {
              console.log("Error :", error);
              response.status(400).json({ message: "Error while inserting otp into database" });
            } else {
              // console.log("sending response ", optmsg);
              const req = formRequest(`Your one time password is ${otp}`, [mobile_no]);
              rp(req).then(res => {
                // console.log("message sent successfully", res);
                response.status(200).json({ message: "otp sent successfully" });
              }).catch(err => {
                console.log("Error while sending message 50 ----", err);
                response.status(400).json({ message: "Error while sending message ***** " });
              });
            }
          });  
        }        
      }
    
    });

    
  } catch (err) {
    response.status(400).json({ message: "Error while inserting otp into database or while sending message" });
  }
}

const verifyOTP = async (request, response) => {
  const { mobile_no, otp } = request.body;
  console.log("verify otp funtion", request.body);
  pool.query(`SELECT EXTRACT(MINUTE FROM (now() - otp_timestamp)) as minute, otp 
              FROM otp_verification where mobile_no='${mobile_no}' and otp=${otp}
              and EXTRACT(MINUTE FROM (now() - otp_timestamp)) <= 2 ORDER BY otp_timestamp DESC
              LIMIT 1`, (error, result) => {
      if (error) {
        return response.status(500).json({ message: "error while validating otp" });
      }
      console.log("res : ", JSON.stringify(result));
      if (result.rowCount === 1) {
        return response.status(200).json({ message: "valid opt number" });
      } else {
        return response.status(404).json({ message: "invalid otp" });
      }
    });
}


const newReferals = (request, response) => {
  const {sender, receivers} = request.body
  var referalLink = request.body.referalLink

  pool.query('SELECT * FROM account WHERE mobile_no = $1 and user_id = $1', [sender], (error, result) => {
      var referCode = null;
      if (error) {
          console.log("error : ", error)
          response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
      } else{
        referCode = result.rows[0].user_refer_code;
        if(!referalLink){
          referalLink = "https://referallink"
        }

        const req = formRequest(`I love Couches cake it's delicious. Use my referral link to get a discount now! ${referalLink}/${referCode} via @couches`, receivers);
        rp(req).then(res => {
           console.log("message sent successfully", res);
            var refralLinkSentCount = 0
           _.each(receivers, function(receiver) {
              refralLinkSentCount = refralLinkSentCount + 1;
              pool.query('INSERT INTO referal_queue (from_no, to_no, refer_code, sent_time) VALUES ($1,$2, $3,now())', [sender,  receiver, referCode], (error, result) => {
                if (error) {
                  console.log("Error :", error);
                  response.status(400).json({ message: "Error while inserting referal entry into database" });
                } else {
                  console.log(receiver);
                }
              });
           });

           if(refralLinkSentCount === receivers.length){
            response.status(200).json({ message: "Refral links sent successfully" });
          }

        }).catch(err => {
          console.log("Error while sending message 50 ----", err);
          response.status(400).json({ message: "Error while sending message ***** " });
        });

      }

       
      //response.status(200).json({"status": "Success", "message":"Order has been placed successfully."});
  })
}

module.exports = {
  sendMessage,
  sendOTP,
  verifyOTP,
  newReferals
};