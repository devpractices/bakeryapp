var pool = require('../../config');
var _ = require('underscore');
var moment = require('moment');

const createProduct = (request, response) => {
    const {name,code,description,categoryId,flavourId, subCategoryId, weightPrice} = JSON.parse(request.body.productdetails);
    var productId = null;

    pool.query('INSERT INTO product (product_name,product_code,product_desc,category_id,flavour_id, sub_category_id, weight_price,created_on) VALUES ($1, $2, $3, $4, $5, $6, $7,now()) RETURNING id', [name,code,description,categoryId,flavourId, subCategoryId, weightPrice], (error, result) => {
      if (error) {
          console.log("error : ", error);
        response.status(500).json({"status": "Fail", "message":"Error occured while creating new product.Please try again."});
      }

      productId = result.rows[0].id;
      if(request.file){
        uploadImage(productId, request.file, response)
      }
      
      response.status(200).json({"status": "Success", "message":"Product created successfully."});
    });
}

function uploadImage(productId, file, res){
    var entityType = 'product',
        entityId = productId,
        imgName = file.filename,
        imgPath = '/productImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType,entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}

const updateProduct = (request, response) => {
    const {name,code,categoryId,weightPrice,id} = request.body;
    
    pool.query("UPDATE product SET product_name = $1,product_code = $2,category_id = $3, weight_price=$4, updated_on='now()' WHERE id = $5",
         [name,code,categoryId,weightPrice,id], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while updating product details.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"Product details has been updated successfully."});
    });
}

const updateProductImage = (request, response) => {
    const {id} = JSON.parse(request.body.productdetails);
    var file = request.file;
    var entityType = 'product',
        entityId = id,
        imgName = file.filename,
        imgPath = '/productImg/' + file.filename;

    var query = "UPDATE images SET img_name = $1, img_path = $2 WHERE entity_id = $3 and entity_type = $4";
    pool.query(query, [imgName, imgPath, entityId, entityType], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while updating product image.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"Product image updated successfully."});
    });
}

const getProducts = (request, response) => {
    const { start, limit} = request.body
    const categoryId = request.body.category;
    var products = [];
    var entityType = 'product';
    // inner join flavour f on f.id = p.flavour_id
    // inner join sub_category sc on sc.id = p.sub_category_id

    var query = `SELECT p.id id, p.product_code, p.product_name,p.product_desc,c.name catgeory_name, p.weight_price, i.img_name, i.img_path, count(p.*) OVER() AS full_count FROM product p
                 inner join category c on c.id = p.category_id
                 left join images i on i.entity_id = p.id and i.entity_type = '${entityType}'`;
    var subQuery = `where p.is_active = true ORDER BY p.id ASC`;
    
    if(categoryId){
        subQuery = ` where p.is_active = true and c.id = ${categoryId}
                    ORDER BY p.id ASC`
    }
    
    
    if(_.has(request.body, "start") && _.has(request.body, "limit")){
        query = `SELECT p.id id, p.product_code, p.product_name,p.product_desc,c.name catgeory_name, p.weight_price, i.img_name, i.img_path, count(p.*) OVER() AS full_count FROM product p
                 inner join category c on c.id = p.category_id
                 left join images i on i.entity_id = p.id and i.entity_type = '${entityType}'`;

        var subQuery = `where p.is_active = true ORDER BY p.id ASC
                        OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`;
        if(categoryId){
            subQuery = ` where p.is_active = true and c.id = ${categoryId}
                        ORDER BY p.id ASC
                        OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
        } 
        
    }

    query = query + subQuery;

    console.log("query : ",query);
    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }
        
        var totalCount = 0;
        if(_.isEmpty(results.rows)){
            response.status(200).json({totalCount: totalCount, products:[]})
        } else{
            _.each(results.rows, function(product) {
        
                // var weightPriceArray = _.map(_.keys(product.weight_price), function(key) {
                //         console.log(" keu ", product.weight_price[key])
                //     return  JSON.parse(product.weight_price[key]) //{ 'weight': key.weight, 'price': data[key].length }
                // });
                
                var productDetails = { 
                    p_id: product.id,
                    productCode: product.product_code,
                    productName: product.product_name,
                    category: product.catgeory_name,
                    imageName: product.img_name,
                    imagePath: product.img_path,
                    weightPrice: product.weight_price,
                    overallRating: 0
                }

               
                totalCount = product.full_count;
                products.push(productDetails);        
                         
            });
            
            respondProducts(response, {totalCount: totalCount, products:products});
        } 
    })
}

function respondProducts(response, produtObjects){
    var pcount = 0;
    _.each(produtObjects.products, function(product) {
        pool.query(`select count(pr.*) reviewcount, SUM(pr.rating) sumofrating from product_review pr where product_id=$1`, [product.p_id] ,function(err, res) {
            pcount = pcount + 1;
            if(!_.isEmpty(res.rows)){
                var resObj = res.rows[0];
                var totalUserRated = parseInt(resObj.reviewcount);
                var sumOfMaxRatingOfReviewCount = (totalUserRated * 5);
                var sumOfRating = parseInt(resObj.sumofrating);
                if(!isNaN(sumOfRating)){
                    product.overallRating = (sumOfRating * 5) / sumOfMaxRatingOfReviewCount;
                }                        
            }

            if(pcount === produtObjects.products.length){
                response.status(200).json(produtObjects)
            }
            
        });
    });
}

const getProductsByOffer = (request, response) => {
    var offerId = parseInt(request.params.offerId);
    const { start, limit} = request.body
    var products = [];
    var entityType = 'product';
    
    //inner join flavour f on f.id = p.flavour_id
    //inner join sub_category sc on sc.id = p.sub_category_id

    var query = `SELECT p.id id, p.product_code, p.product_name,p.product_desc, p.weight_price,c.name catgeory_name, i.img_name, i.img_path, po.offer_id, po.from_date, po.to_date, count(p.*) OVER() AS full_count FROM product p
                inner join category c on c.id = p.category_id
                inner join product_offer po on po.product_id = p.id
                left join images i on i.entity_id = p.id and i.entity_type = '${entityType}'
                where po.offer_id =${offerId}
                ORDER BY p.id ASC`;
    
    if(_.has(request.body, "start") && _.has(request.body, "limit")){
        query = `SELECT p.id id, p.product_code, p.product_name,p.product_desc, p.weight_price,c.name catgeory_name, i.img_name, i.img_path, po.offer_id, po.from_date, po.to_date, count(p.*) OVER() AS full_count FROM product p
                inner join category c on c.id = p.category_id
                inner join product_offer po on po.product_id = p.id
                left join images i on i.entity_id = p.id and i.entity_type = '${entityType}'
                where po.offer_id =${offerId}
                ORDER BY p.id ASC
                OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`;
    }

    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }


        var totalCount = 0;
        if(results && !_.isEmpty(results.rows)){
            _.each(results.rows, function(product) {
                var productDetails = { 
                    p_id: product.id,
                    productCode: product.product_code,
                    productName: product.product_name,
                    // productDesc: product.product_desc,
                    // flavour: product.flavour_name,
                    category: product.catgeory_name,
                    // sub_category: product.sub_catgeory_name,
                    // price: product.price,
                    weightPrice: product.weight_price,
                    imageName: product.img_name,
                    imagePath: product.img_path,
                    offerId: product.offer_id,
                    offerValidFrom : moment(product.from_date).format('YYYY-MM-DD'), 
                    offerValidTo : moment(product.to_date).format('YYYY-MM-DD')
                }

                totalCount = product.full_count;
                products.push(productDetails);
            });
        }
        
          
        response.status(200).json({totalCount : totalCount , products:products})
    })
}
  
const getProductById = (request, response) => {
    const id = parseInt(request.params.id)
    var entityType = 'product';

    // inner join flavour f on f.id = p.flavour_id
    // inner join sub_category sc on sc.id = p.sub_category_id
    var query = `SELECT p.id id, p.product_code, p.product_name,p.product_desc, p.weight_price,c.name catgeory_name, i.img_name, i.img_path FROM product p
                inner join category c on c.id = p.category_id
                left join images i on i.entity_id = p.id and i.entity_type = $2
                WHERE p.id = $1
                ORDER BY p.id ASC`;

    pool.query(query, [id, entityType], (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var productDetails = {};

        _.each(results.rows, function(product) {
            productDetails = { 
                p_id: product.id,
                productCode: product.product_code,
                productName: product.product_name,
                // productDesc: product.product_desc,
                // flavour: product.flavour_name,
                category: product.catgeory_name,
                // sub_category: product.sub_catgeory_name,
                // price: product.price,
                imageName: product.img_name,
                imagePath: product.img_path,
                weightPrice: product.weight_price,
                overallRating: 0
            }

            pool.query(`select count(pr.*) reviewcount, SUM(pr.rating) sumofrating from product_review pr where product_id=$1`, [product.id] ,function(err, res) {
                
                if(!_.isEmpty(res.rows)){
                    
                    var resObj = res.rows[0];
                    var totalUserRated = parseInt(resObj.reviewcount);
                    var sumOfMaxRatingOfReviewCount = (totalUserRated * 5);
                    var sumOfRating = parseInt(resObj.sumofrating);
                    productDetails.overallRating = (sumOfRating * 5) / sumOfMaxRatingOfReviewCount
                    
                }
            });            
        });
        //response.status(200).json(productDetails)
        getReviews(response, productDetails);
    })
}

function getReviews(response, product){
    var query = `SELECT r.id as "reviewId", r.review, a.name, r.rating from product_review r
                        inner join account a on a.id = r.user_id
                        where r.product_id = ${product.p_id}`;

            pool.query(query, (error, results) => {
                if (error) {
                    response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
                }

                product["reviews"] = []
                _.each(results.rows, function(reviewObj) {
                    var reviewsDetails = { 
                        id: reviewObj.reviewId,
                        review: reviewObj.review,
                        userName: reviewObj.name,
                        rating: reviewObj.rating
                    }
                    product.reviews.push(reviewsDetails);            
                });

                response.status(200).json(product)
            });

            
}

function getOffersAndReviews(response, product){
    var entityType = 'offer';
    var offers= []
    var query = `SELECT o.id, o.offer_value, o.offer_type, o.offer_desc, po.product_id, po.from_date, po.to_date, i.img_name, i.img_path, count(o.*) OVER() AS full_count FROM offer o
                inner join product_offer po on po.offer_id = o.id
                left join images i on i.entity_id = o.id and i.entity_type = '${entityType}'
                where po.product_id = ${product.p_id}
                ORDER BY o.id ASC`;

    pool.query(query, (error, results) => {
                if (error) {
                    response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
                }
            
                product["offers"] = [];
                _.each(results.rows, function(offer) {
                    var offerDetails = { 
                        o_id: offer.id,
                        offerValue: offer.offer_value,
                        offerType: offer.offer_type,
                        offerDesc: offer.offer_desc,
                        productId: offer.product_id,
                        imageName: offer.img_name,
                        imagePath: offer.img_path,
                        offerValidFrom : moment(offer.from_date).format('YYYY-MM-DD'), 
                        offerValidTo : moment(offer.to_date).format('YYYY-MM-DD')
                    }
                    product.offers.push(offerDetails);            
                });
            });

            var query = `SELECT r.id  reviewId, r.review, a.name, r.rating from product_review r
                        inner join account a on a.id = r.user_id
                        where r.product_id = ${product.p_id}`;

            pool.query(query, (error, results) => {
                if (error) {
                    response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
                }

                product["reviews"] = []
                _.each(results.rows, function(reviewObj) {
                
                    var reviewsDetails = { 
                        id: reviewObj.reviewId,
                        review: reviewObj.review,
                        userName: reviewObj.name,
                        rating: reviewObj.rating
                    }
                    product.reviews.push(reviewsDetails);            
                });

                response.status(200).json(product)
            });
}

const createReview = (request, response) => {
    const {userId, productId, review, rating} = request.body;
    
    pool.query('INSERT INTO product_review (user_id,product_id,review, rating, created_on) VALUES ($1, $2, $3, $4,now()) RETURNING id', [userId, productId, review, rating], (error, result) => {
      if (error) {
        console.log(" error : ", error);
        response.status(500).json({"status": "Fail", "message":"Error occured while adding product review.Please try again."});
      }

      if(result){
        response.status(200).json({"status": "Success", "message":"Product review updated successfully."});
      }
      
    });
}

const updateReview = (request, response) => {
    const {reviewId, review, rating} = request.body;
    
    pool.query('update product_review set review = $2, rating = $3, updated_on= now() where id=$1', [reviewId, review, rating], (error, result) => {
      if (error) {
        console.log(" error : ", error);
        response.status(500).json({"status": "Fail", "message":"Error occured while updateing product review.Please try again."});
      }

      if(result){
        response.status(200).json({"status": "Success", "message":"Product review updated successfully."});
      }
      
    });
}

const deleteProduct = (request, response) => {
    var productId =  parseInt(request.params.id);
    var checkProductInOrder = `select count(ot.*) orderactivecount from order_items ot 
                               inner join orders o on o.id = ot.order_id and o.status in ('INITIATE', 'INPROGRESS')
                               where ot.product_id = ${productId}`
    
    pool.query(checkProductInOrder,(checkError, checkResult) => {
        if (checkError) {
            response.status(500).json({"status": "Fail", "message":"Error occured while checking product is in active order.Please try again."});
        }      
        
        if(!_.isEmpty(checkResult.rows)){
            if(parseInt(checkResult.rows[0].orderactivecount) > 0){
                response.status(500).json({"status": "Fail", "message":"This product has some orders in queue. Please change the status of order and try again"});
            } else {
                pool.query("UPDATE product SET is_active = false WHERE id = $1",
                    [productId], (error, result) => {
                    if (error) {
                        response.status(500).json({"status": "Fail", "message":"Error occured while deleting product.Please try again."});
                    }      
                    response.status(200).json({"status": "Success", "message":"Product deleted successfully."});
                });
            }
        } else {
            response.status(500).json({"status": "Fail", "message":"Unexpected erorr"});
        }
    });    
}

const deleteReview = (request, response) => {
    var reviewId =  parseInt(request.params.id);
    pool.query("delete from product_review WHERE id = $1",
        [reviewId], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while deleting review.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"Review deleted successfully."});
    });
}

module.exports = {
    createProduct,
    updateProduct,
    createReview,
    updateProductImage,
    getProducts,    
    getProductById,
    getProductsByOffer,
    deleteProduct,
    updateReview,
    deleteReview
}