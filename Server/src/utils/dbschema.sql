-- Role Master Table
CREATE TABLE role (
        id serial PRIMARY KEY,
        role_name VARCHAR (50) UNIQUE NOT NULL,
        created_on TIMESTAMP NOT NULL,
        updated_on TIMESTAMP
);

INSERT INTO role(role_name, created_on)
VALUES ('Admin', now());
INSERT INTO role(role_name, created_on)
VALUES ('User', now());
INSERT INTO role(role_name, created_on)
VALUES ('Super Admin', now());

-- Account / User Maste Table
CREATE TYPE gender AS ENUM ('male', 'female', 'other');
CREATE TABLE account(
        id serial PRIMARY KEY,
        name VARCHAR (200) NOT NULL,
        user_id VARCHAR (50) UNIQUE NOT NULL,
        email VARCHAR (150) UNIQUE,
        date_of_birth date,
        mobile_no VARCHAR (30) UNIQUE NOT NULL,
        gender gender,
        created_on TIMESTAMP NOT NULL,
        last_login TIMESTAMP,        
        is_active BOOLEAN DEFAULT true,
        is_deleted BOOLEAN DEFAULT false,
        password VARCHAR (400),
        iv VARCHAR (400),
        envkey VARCHAR (400),
        updated_on TIMESTAMP,
        user_refer_code VARCHAR (200),
        CONSTRAINT refer_code_unique UNIQUE (user_refer_code)
);

CREATE TABLE account_address (
    id serial PRIMARY KEY,
    user_id integer REFERENCES account(id) NOT NULL,
    address VARCHAR (500) NOT NULL,
    city VARCHAR (40),
    state VARCHAR (40),
    country VARCHAR (40),
    pincode integer,
    mobile_no VARCHAR (30),
    address_type VARCHAR (40) DEFAULT 'home', -- 'home', 'billing', 'shipping'
    is_active BOOLEAN DEFAULT true,
    created_on TIMESTAMP NOT NULL,
    is_default BOOLEAN DEFAULT false,
    updated_on TIMESTAMP
);

-- Account - Role Relation Table	
CREATE TABLE account_role (
        user_id integer NOT NULL,
        role_id integer NOT NULL,
        grant_date timestamp without time zone,
        PRIMARY KEY (user_id, role_id),
        CONSTRAINT account_role_role_id_fkey FOREIGN KEY (role_id)
            REFERENCES role (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT account_role_user_id_fkey FOREIGN KEY (user_id)
            REFERENCES account (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE reward_and_recognition (
    id serial PRIMARY KEY,
    sender_id integer REFERENCES account(id) NOT NULL,
    reciever_id integer REFERENCES account(id) NOT NULL,
    is_active BOOLEAN DEFAULT true,
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);

-- Category Master Table
CREATE TABLE category (
        id serial PRIMARY KEY,
        name VARCHAR (255) UNIQUE NOT NULL,
        created_on TIMESTAMP NOT NULL,
        updated_on TIMESTAMP
);

-- Subcategory Master Table
CREATE TABLE sub_category (
        id serial PRIMARY KEY,
        name VARCHAR (255) UNIQUE NOT NULL,
        parent integer REFERENCES category (id),
        created_on TIMESTAMP NOT NULL,
        updated_on TIMESTAMP
);

-- Flavour Master Table
CREATE TABLE flavour (
        id serial PRIMARY KEY,
        name VARCHAR (255) UNIQUE NOT NULL,
        created_on TIMESTAMP NOT NULL,
        updated_on TIMESTAMP
);

-- Product Master Table
CREATE TABLE product (
        id serial PRIMARY KEY,
        product_code VARCHAR (255),
        product_name VARCHAR (255) NOT NULL,
        product_desc VARCHAR (400),  -- NOT NULL,
        flavour_id integer REFERENCES flavour (id),
        category_id integer REFERENCES category (id),
        sub_category_id integer REFERENCES sub_category (id),
        weight_price varchar ARRAY,
        created_on TIMESTAMP NOT NULL,
        is_active BOOLEAN DEFAULT true,
        updated_on TIMESTAMP
);

CREATE TABLE product_review (
    id serial PRIMARY KEY,
    user_id integer REFERENCES account(id) NOT NULL,
    product_id integer REFERENCES product(id) NOT NULL,
    review VARCHAR (500) NOT NULL, 
    rating integer NOT NULL,
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP,
    CONSTRAINT user_product_unique UNIQUE (user_id, product_id)
);

-- Offer Master Table
CREATE TABLE offer (
        id serial PRIMARY KEY,
        offer_value integer, -- value how discount will apply depend on type
        offer_type VARCHAR (50) , -- "percent" or "flat" or "points"
        offer_desc VARCHAR (255),
        offer_code VARCHAR (50) NOT NULL, 
        created_on TIMESTAMP NOT NULL,
        is_active BOOLEAN DEFAULT true,
        from_date date NOT NULL,
        to_date date NOT NULL,
        point integer,
        updated_on TIMESTAMP
);

-- ALTER TABLE offer ADD COLUMN point integer;

-- Entity wise Image Table
CREATE TABLE images (
    id serial PRIMARY KEY,
    entity_type VARCHAR (255) NOT NULL,  --  "Product" or "offer" or user
    entity_id integer NOT NULL, -- "Product_id" or "Offer_id" or "user_id"
    img_name VARCHAR (255),
    img_path VARCHAR (255),
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);

-- order Master Table
CREATE TABLE orders (
    id serial PRIMARY KEY,
    user_id integer REFERENCES account(id) NOT NULL,
    payment_mode VARCHAR (255) NOT NULL, 
    payment_status VARCHAR (255) NOT NULL, 
    offer_id integer REFERENCES offer(id),
    transaction_num VARCHAR (255), 
    payment_date TIMESTAMP NOT NULL,
    status VARCHAR (255) NOT NULL,
    total_amount decimal(12,2), 
    order_date TIMESTAMP NOT NULL,
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP  
);


CREATE TABLE order_items (
    order_id integer REFERENCES orders(id) NOT NULL,
    product_id integer REFERENCES product(id) NOT NULL,
    quantity integer,
    weight varchar(40),
    price decimal(12,2),
    delivery_date TIMESTAMP,
    updated_on TIMESTAMP,
    occasion VARCHAR(255)
);

-- Refral system

CREATE TABLE refer_type (
    id serial PRIMARY KEY,
    type VARCHAR (255) NOT NULL, -- sender, reciever
    points integer NOT NULL,
    created_on TIMESTAMP NOT NULL,
    is_active BOOLEAN DEFAULT true
);
INSERT INTO refer_type(type, points, created_on)
VALUES ('reciever', 20, now());
INSERT INTO refer_type(type, points, created_on)
VALUES ('sender', 20, now());



CREATE TABLE credit_randr_point (
    id serial PRIMARY KEY,
    randr_id integer REFERENCES reward_and_recognition(id),
    user_id integer REFERENCES account(id) NOT NULL,
    refer_type_id integer REFERENCES refer_type(id),
    points integer NOT NULL,
    created_on TIMESTAMP NOT NULL
);

-- Following table are not in use
CREATE TABLE product_offer (
        product_id integer REFERENCES product (id),
        offer_id integer REFERENCES offer (id),
        from_date date NOT NULL,
        to_date date NOT NULL,
        created_on TIMESTAMP NOT NULL,
        updated_on TIMESTAMP
);


-- SMS integration tables

CREATE TABLE login_details (
    mobile_no VARCHAR (30) REFERENCES account (mobile_no) NOT NULL,
    login_time TIMESTAMP NOT NULL,
    login_status VARCHAR(2)
);

CREATE TABLE sms_details (
    from_no VARCHAR (30) REFERENCES account (mobile_no) NOT NULL,
    sms_type VARCHAR(3),
    sent_time TIMESTAMP NOT NULL,
    to_no VARCHAR (500)
);

CREATE TABLE otp_verification (
    id serial PRIMARY KEY,
    mobile_no VARCHAR (30) REFERENCES account (mobile_no) NOT NULL,
    otp integer NOT NULL,
    otp_timestamp TIMESTAMP NOT NULL
);

CREATE TABLE referal_queue (
    id serial PRIMARY KEY,
    from_no VARCHAR (30) REFERENCES account (mobile_no) NOT NULL,
    to_no VARCHAR (30),
    sent_time TIMESTAMP NOT NULL,
    refer_code VARCHAR (200)
);

CREATE TABLE template_message (
    id serial PRIMARY KEY,
    msg VARCHAR NOT NULL,
    msg_type VARCHAR (30) NOT NULL,
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);


-- ALTER TABLE order_items ADD COLUMN name_on_cake VARCHAR;
-- ALTER TABLE account_address ADD COLUMN is_default BOOLEAN DEFAULT false;

-- ALTER TABLE order_items ADD COLUMN delivery_date TIMESTAMP;

-- ALTER TABLE order_items DROP column mobile_no;
-- ALTER TABLE account_address ADD COLUMN mobile_no VARCHAR (30);

-- ALTER TABLE order_items ADD COLUMN occasion VARCHAR;


-- ALTER TABLE account Add COLUMN password VARCHAR (400);
-- ALTER TABLE account Add COLUMN iv VARCHAR (400);
-- ALTER TABLE account Add COLUMN envkey VARCHAR (400);

-- ALTER TABLE product ADD COLUMN is_active BOOLEAN DEFAULT true;