const Pool = require('pg').Pool
const pool = new Pool({
  user: 'root',
  host: 'localhost',
  database: 'bakery',
  password: 'password',
  port: 5432,
})

pool.connect(function (err, client, done) {
    //if (err) throw new Error(err);
    if(err){
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
    
}); 

module.exports = pool;