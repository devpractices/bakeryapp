var express = require('express');
var router = express.Router();
var multer = require('multer');
var users = require('../src/controllers/user')

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });


// User - Create , Update , Delete, user list, user by id
var userStorage = multer.diskStorage({
  destination: function(req, file, callback) {
      callback(null, './public/images/user');
  },
  filename: function(req, file, callback) {
      callback(null, Date.now() + "_" + file.originalname);
  }
});

var uploadUser = multer({
    storage: userStorage
}).single('fileInput');

router.post('/create', uploadUser, users.createUser)
router.post('/createAdmin', users.createAdminUser)
router.post('/add/address', users.addAddress)
router.delete('/address/delete/:id', users.deleteAddress)
router.post('/updateImage', uploadUser, users.updateUserImage)
router.post('/update', users.updateUser)
router.post('/address/update', users.updateAddress)
router.delete('/delete/:id', users.deleteUser)
router.post('/list', users.getUsers)
router.get('/:id', users.getUserById)
router.post('/login', users.login)
router.post('/details', users.getUserDetails)
router.get('/address/:id', users.getUserAddress)
router.get('/rewardPoints/:id', users.getRewardPoints)
router.get('/reactivate/:id', users.reactivateUser)
router.post('/changeDefault', users.changeDefault)



module.exports = router;
