var express = require('express');
var router = express.Router();
var roles = require('../src/controllers/role')

router.post('/create', roles.createRole)

module.exports = router;