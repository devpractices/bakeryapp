var express = require('express');
var router = express.Router();
var multer = require('multer');
var masters = require('../src/controllers/masters')

// For accessing product images 
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/images/category');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
});
  
var upload = multer({
storage: Storage
}).single('fileInput')

router.post('/category/create', upload ,masters.createCategory)
router.get('/categories', masters.getCategories)
router.post('/subcategory/create', masters.createSubCategory)
router.get('/subcategories', masters.getSubCategories)
router.get('/subcategories/:parent', masters.getSubCategoriesById)
router.post('/flavour/create', masters.createFlavour)
router.get('/flavours', masters.getFlavours)
router.post('/referalType/create', masters.referPointTypeCreate)
router.get('/getCounts', masters.getCounts)


module.exports = router;