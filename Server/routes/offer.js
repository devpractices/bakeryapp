var express = require('express');
var router = express.Router();
var multer = require('multer');
var offer = require('../src/controllers/offer')

// Offer - Create , Update , Delete, user list
var offerStorage = multer.diskStorage({
  destination: function(req, file, callback) {
      callback(null, './public/images/offer');
  },
  filename: function(req, file, callback) {
      callback(null, Date.now() + "_" + file.originalname);
  }
});

var uploadOffer = multer({
  storage: offerStorage
}).single('fileInput');

router.post('/create', uploadOffer ,offer.createOffer)
router.post('/update', offer.updateOffer)
router.post('/updateImage', uploadOffer, offer.updateOfferImage)
router.post('/list', offer.getOffers)
router.get('/:id', offer.getOfferById)
router.post('/map', offer.mapOfferProduct)
// router.post('/:productId', offer.getOffersByProduct) // Get offers by product id
router.post('/validate', offer.validateOfferCode)
router.delete('/:id', offer.deleteOffer)

module.exports = router;