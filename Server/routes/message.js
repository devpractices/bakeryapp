var express = require('express');
var router = express.Router();
var message = require('../src/controllers/message');

router.post('/msg', message.sendMessage);
router.post('/sendotp', message.sendOTP);
router.post('/verifyotp', message.verifyOTP);
router.post('/newReferals', message.newReferals);


module.exports = router;