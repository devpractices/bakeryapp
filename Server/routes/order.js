var express = require('express');
var router = express.Router();
var orders = require('../src/controllers/order')

// Order - Create , Update , Delete, user list, user by id
router.post('/create', orders.createOrder)
router.post('/statusUpdate', orders.updateOrderStatus)
router.post('/list', orders.getOrders)
router.get('/all', orders.getAllOrders)
router.get('/pending', orders.pendingOrders)


// router.post('/order/history', orders.getOrderHistory)
router.get('/:id/:userId', orders.getOrderById)

module.exports = router;