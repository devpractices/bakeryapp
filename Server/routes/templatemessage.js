var express = require('express');
var router = express.Router();
var templateMessage = require('../src/controllers/templatemessage');


router.post('/create', templateMessage.createTemplateMessage);
router.post('/update', templateMessage.updateTemplateMessage);
router.get('/', templateMessage.getTemplateMessages);
router.get('/:id', templateMessage.getTemplateMessageById);


module.exports = router;